# Wonder-Reader

[![dependencies Status](https://david-dm.org/alice-em/wonder-reader/status.png)](https://david-dm.org/alice-em/wonder-reader)
[![GitHub version](https://badge.fury.io/gh/alice-em%2Fwonder-reader.svg)](https://badge.fury.io/gh/alice-em%2Fwonder-reader)
`v1.0.0a`

Running Electron 2.0.2
## Downloads
Project will be compiled and released for Windows, OSX, and Linux after testing.

## Installation and Starting
__Requirements__: `git`, `node`, and `npm`

```shell
git clone https://github.com/alice-em/wonder-weader.git
cd wonder-reader
npm install
npm run electron-dev
```

##### Compiling
__Requirements__: electron-packager

##### Terminal
* Go to where you cloned __Wonder Reader__

````shell
cd /usr/BoosterGold/home/git/wonder-reader
npm install
npm run packager
````

## Development
__Requirements__: A love for comics

##### TODO:
* Any other neat ideas that could work go here too.
* Double click zoom in function!
* Comics at either __firstPage__ or __lastPage__ can open up the next file in library folder.

##### Technologies
* React :: https://reactjs.org/
* Material UI :: https://material-ui.com/
* Electron :: http://electron.atom.io
* Node :: https://nodejs.org/en/
* Font Awesome :: http://fontawesome.io/
* Dragscroll.js :: https://github.com/asvd/dragscroll
